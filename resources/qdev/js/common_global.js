$(async () => {

    window._uris = await geturis();
    if ($(window).width() < 768) {
        $('[data-toggle="tooltip"]').tooltip('disable');
    } else {
        $('[data-toggle="tooltip"]').tooltip();
    }

});


window.geturis = async () => {

    let _token = $('meta[name=csrf-token]').attr('content');
    let base_url = $('meta[name=base-url]').attr('content');
    base_url += '/uriforbetterlife'

    let uris = await $.ajax({
        type: "POST",
        url: base_url,
        data: {
            _token: _token
        },
        dataType: "json",
        success: function (response) {
            return response
        },
        error: (xhr, status, error) => { }
    });
    return uris;
}
