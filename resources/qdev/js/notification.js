// notification toastr
window.notify = (message = "Something happened!", type = "info", title = false) => {

    switch (type) {
        case 'success':
            toastr.success(message, title);
            break;
        case 'info':
            toastr.info(message, title);
            break;
        case 'warning':
            toastr.warning(message, title);
            break;
        case 'error':
            toastr.error(message, title);
            break;
        default:
            toastr.info(message, title);
            break;
    }
}

$(() => {
    let is_notification = $('body').attr('data-notification');
    if ( is_notification.length > 0 && !!is_notification) {
        let message = $('body').attr('data-notification-message');
        let title = $('body').attr('data-notification-title');
        let type = $('body').attr('data-notification-type');
        notify(message, type, title);
    }
})
