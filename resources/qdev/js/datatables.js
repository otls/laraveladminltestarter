window.renderDatatable = (config = {}, selector = '.table') => {
    let defaultConfig = {
        'serverside'    : true,
        'paging'        : true,
        'ordering'      : true,
        'searching'     : true,
        'processing'    : true,
        'responsive'    : true,
        'autoWidth'     : true,

    }

    let drawCallback = config.drawCallback();
    config = {
        ...defaultConfig,
        ...config
    }

    config.drawCallback = (setting) => {
        if (typeof setting == 'function') {
            drawCallback(setting);
            console.log("funtion oke");
        }

    }

    let datatable = $(selector).DataTable(config);
    return datatable;
}
